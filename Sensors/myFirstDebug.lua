local sensorInfo = {
	name = "myFirstDebug",
	desc = "Sends data to example debug widget",
	author = "DarioLanza",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
	if #units > 0 then
		local unitID = units[1]
		local x,y,z = Spring.GetUnitPosition(unitID)
		local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
		if (Script.LuaUI('myFirstDebug_update')) then
			Script.LuaUI.myFirstDebug_update(
				unitID, -- key
				{	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z) + Vec3(dirX*strength, dirY*strength, dirZ*strength)
				}
			)
		end
		return {	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z) + Vec3(dirX*strength, dirY*strength, dirZ*strength)
				}
	end
end